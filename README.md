# README #

This project is build with maven, it has a java back-end and an AngularJs client.

### What is this repository for? ###

- Website that can scan US/Canadian drivers license barcodes with your webcam (or camera phone).

### How do I get set up? ###

Build: mvn compile
Test: mvn test