package ca.goudsbloem.willem.barcode;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import ca.goudsbloem.willem.barcode.DriversLicenseBarcodeReader;

import com.google.zxing.Result;

public class DriversLicenseBarcodeReaderTest {

	DriversLicenseBarcodeReader dlbcr;
	
	@Before
	public void setUp() throws Exception {
		dlbcr = new DriversLicenseBarcodeReader();
	}

	@Test
	public void testGetResultByFileLocation() {
		Result result = null;
		try {
			result = dlbcr.getResult("barcodes/dl-wg-horizontal.png");
		} catch (Exception e) {
			fail(e.getCause() + e.getMessage());
		} 
		assertNotNull(result);
	}
	
	@Test
	public void testFailGetResultByFileLocation() {
		int i = 0;
		try {
			dlbcr.getResult("barcodes/null.png");
		} catch (IOException e) {
			i = -1;
		}
		assertEquals(i, -1);
	}
	
	@Test
	public void testFailGetResultByFileLocationBadImage() {
		int i = 0;
		try {
			dlbcr.getResult("barcodes/wg-dl-fail.jpg");
		} catch (IOException e) {
			i = -1;
		}
		assertEquals(i, -1);
	}
	
	@Test
	public void testGetResultByInputStream() {
		Result result = null;
		try {
			FileInputStream fis = new FileInputStream("barcodes/dl-wg-horizontal.png");
			result = dlbcr.getResult(fis);
		} catch (Exception e) {
			fail(e.getCause() + e.getMessage());
		} 
		assertNotNull(result);
	}
	
	

}
