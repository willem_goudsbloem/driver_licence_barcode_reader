angular.module("app", [])
  .directive("camera", ["endPoint", "dataURItoBlob", "$http", function(endPoint, dataURItoBlob, $http) {
    return {
      replace: true,
      template: '<div><video autoplay="true">video?</video><canvas width="{{width}}" height="{{height}}"></canvas><p><button ng-click="stop()" class="btn btn-primary">{{videoStatus}}</button><button ng-click="record()" class="btn btn-danger">record</button><select ng-model="sel" ng-options="src.label for src in videoSources" ></select></p></div>',
      link: function(scope, elem, attr) {
        scope.width = 0;
        scope.height = 0;
        scope.videoStatus = 'stop';
        scope.videoSources = [];
        var video = elem[0].childNodes[0];
        var canvas = elem[0].childNodes[1];

        MediaStreamTrack.getSources(gotSources);

        function gotSources(info) {
          for (var i = 0; i < info.length; i++) {
            if (info[i].kind === "video") {
              console.debug(info[i]);
              scope.videoSources.push({
                "label": info[i].label || "video " + (scope.videoSources.length + 1),
                "id": info[i].id
              });
            }
          }
          scope.sel = scope.videoSources[0];
          scope.$apply();
          start();
        }

        scope.$watch('sel', function(){
          if (scope.sel) {
            console.debug(scope.sel);
            start();
          }
        });

        function start() {
          var contraints = {
            video: {
              optional: [{
                sourceId: scope.sel.id
              }],
              mandatory: {
                minWidth: 640,
                minHeight: 480
              }
            },
            audio: false
          };
          navigator.webkitGetUserMedia(contraints, success, error);
        }


        function success(stream) {
          video.src = window.URL.createObjectURL(stream);
          video.play();
        }

        function error(e) {
          console.error(e);
        }

        scope.record = function() {
          scope.height = video.videoHeight;
          scope.width = video.videoWidth;
          var ctx = canvas.getContext('2d');
          var i = 0;
          timer = setInterval(function() {
            if (++i > 200) clearInterval(timer);
            ctx.drawImage(video, 0, 0, scope.width, scope.height);
            var data = canvas.toDataURL('image/jpeg', 1.0);
            var blob = dataURItoBlob(data);
            $http.post(location.protocol + "/api", blob).
            success(function(data, status, headers, config) {
            	clearInterval(timer);
            	console.debug("DATA FOUND: "+ data);
            	alert(data);
              }).
              error(function(data, status, headers, config) {
              });
          }, 250);
        };
        scope.stop = function() {
          stream.stop();
          scope.videoStatus = 'stopped';
        };

      }
    };
  }])
  .service("dataURItoBlob", function() {
    return function(dataURI) {
      // convert base64/URLEncoded data component to raw binary data held in a string
      var byteString;
      if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
      else
        byteString = unescape(dataURI.split(',')[1]);

      // separate out the mime component
      var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

      // write the bytes of the string to a typed array
      var ia = new Uint8Array(byteString.length);
      for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }
      return new Blob([ia], {
        type: mimeString
      });
    };
  })
  .service("endPoint", ["$http", function($http) {
    return {
      send: function(data) {
        $http.post(location.protocol + "/api", data).
        success(function(data, status, headers, config) {
        	return data;
          }).
          error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
      }
    };
  }]);
