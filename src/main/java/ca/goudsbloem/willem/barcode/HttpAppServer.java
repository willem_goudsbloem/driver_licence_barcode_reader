package ca.goudsbloem.willem.barcode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsServer;

@SuppressWarnings("restriction")
public class HttpAppServer {

	static Logger logger = Logger.getLogger("BasicHttpServer");

	public static void main(String[] args) throws Exception {
		new HttpAppServer().run();
	}

	public void run() throws Exception {
		HttpsServer server = HttpsServer.create(new InetSocketAddress(443), 5);
		SSLContext sslContext = SSLContext.getInstance("TLS");
		// keystore
		char[] keystorePassword = "barcode".toCharArray();
		KeyStore ks = KeyStore.getInstance("JKS");
		ks.load(this.getClass().getClassLoader()
				.getResourceAsStream("ssl/server_keystore.ks"),
				keystorePassword);
		KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
		kmf.init(ks, keystorePassword);
		sslContext.init(kmf.getKeyManagers(), null, null);
		HttpsConfigurator configurator = new HttpsConfigurator(sslContext);
		server.setHttpsConfigurator(configurator);
		server.createContext("/", new Handler());
		server.setExecutor(null); // creates a default executor
		server.start();

		logger.log(Level.INFO, "server running on port: "
				+ server.getAddress().getPort());
	}

	class Handler implements HttpHandler {

		static final String htmlRoot = "public/";

		@Override
		public void handle(HttpExchange t) throws IOException {
			logger.log(Level.INFO, t.getRequestURI().getPath().toString());
			switch (t.getRequestURI().getPath()) {
			case "/api":
				api(t);
				break;
			default:
				html(t);

			}
		}

		private void html(HttpExchange t) throws IOException {
			t.getResponseHeaders().add("Access-Control-Allow-Methods",
					"GET,OPTIONS");
			t.getResponseHeaders().add("Access-Control-Allow-Headers",
					"Origin, X-Requested-With, Content-Type, Accept");
			String path = htmlRoot + t.getRequestURI().getPath().toString();
			File file = new File(path);
			// logger.log(Level.INFO, file.isFile() + "");
			logger.log(Level.INFO, file.getAbsolutePath() + "");
			InputStream in = this.getClass().getClassLoader()
					.getResourceAsStream(path);
			byte[] b = new byte[in.available()];
			in.read(b);
			in.close();
			t.sendResponseHeaders(200, b.length);
			OutputStream os = t.getResponseBody();
			os.write(b);
			os.close();
		}

		private void api(HttpExchange t) throws IOException {
			logger.log(Level.INFO, t.getRequestMethod());
			t.getResponseHeaders().add("Access-Control-Allow-Methods",
					"POST,OPTIONS");
			t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
			t.getResponseHeaders().add("Access-Control-Allow-Headers",
					"Origin, X-Requested-With, Content-Type, Accept");
			String response = "";
			if (t.getRequestMethod().equals("POST")) {
				// writeToFile(t.getRequestBody());
				response = barcodeReader.getString(t.getRequestBody()); 
				System.out.println(response);
			}
			t.sendResponseHeaders(200, response.length());
			OutputStream os = t.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}

		DriversLicenseBarcodeReader barcodeReader = new DriversLicenseBarcodeReader();


		private void writeToFile(InputStream in) {
			try {
				// dlbr.getResult(bb.array());
				FileOutputStream fos = new FileOutputStream(
						"barcodes/my-image3-" + (new Date()).getTime() + ".png");
				byte[] b = new byte[1024 * 8];
				int read;
				while ((read = in.read(b)) != -1) {
					fos.write(b, 0, read);
					fos.flush();
				}
				fos.close();
			} catch (IOException e) {
				logger.log(Level.SEVERE, e.getMessage());
			}
		}
	}

}
