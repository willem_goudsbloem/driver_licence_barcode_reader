package ca.goudsbloem.willem.barcode;

import java.util.logging.Logger;

public class BarcodeReaderException extends Exception {
	
	private static final long serialVersionUID = 2270819400543323169L;
	
	Logger logger = Logger.getLogger("ExceptionLogger");
	
	public BarcodeReaderException(Exception e) {
		super(e);
	}
	
	public BarcodeReaderException(String msg) {
		super(msg);
	}

}
