package ca.goudsbloem.willem.barcode;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;

public class Main {

	static Map<String, String> filter = new HashMap<String, String>();
	// static File file = new
	// File("C://eclipse_projects//eclipse_luna_java//poc-barcode//barcodes//my-image.png");

	static DriversLicenseBarcodeReader barcodeReader = new DriversLicenseBarcodeReader();

	static {
		filter.put("DBA", "expiry date");
		filter.put("DCS", "last name");
		filter.put("DCT", "first name");
	}

	public static void main(String[] args) {
		File[] files = getFiles("my-image");
		for (File file : files) {
			System.out.println(file.getName());
			BufferedImage image;
			try {
				image = ImageIO.read(file);
				Result result = barcodeReader.getResult(image);
				if (result != null) {
					print(result);
					System.out.println(result);
					System.exit(-1);
				} else {
					System.out.println("No barcode has been found");
				}
			} catch (IOException e) {

			}
		}
	}

	private static void print(Result result) {
		System.out.println("format: " + result.getBarcodeFormat());
		System.out.println("error correction level: "
				+ result.getResultMetadata().get(
						ResultMetadataType.ERROR_CORRECTION_LEVEL));
		System.out.println(result);
		String[] ra = result.toString().split("\n");
		for (String val : ra) {
			for (String f : filter.keySet()) {
				if (val.startsWith(f)) {
					System.out.printf(filter.get(f) + ":\t%s%n",
							val.substring(3));
				}
			}
		}
	}

	private static File[] getFiles(String prefix) {
		File dir = new File("barcodes");
		File[] files = dir.listFiles();
		ArrayList<File> filtered = new ArrayList<File>();
		for (File file : files) {
			if (file.getName().startsWith(prefix)) {
				filtered.add(file);
			}
		}
		return filtered.toArray(new File[filtered.size()]);
	}

}
