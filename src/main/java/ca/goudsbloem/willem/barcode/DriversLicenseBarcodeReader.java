package ca.goudsbloem.willem.barcode;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.pdf417.PDF417Reader;
import com.jhlabs.image.SharpenFilter;

public class DriversLicenseBarcodeReader {

	private Logger logger = Logger.getLogger("DriverLicenseBarcodeReader");

	private PDF417Reader reader = new PDF417Reader();

	public DriversLicenseBarcodeReader() {
		super();
	}

	public Result getResult(String imageFileUri) throws IOException {
		BufferedImage image = ImageIO.read(new File(imageFileUri));
		return getResult(image);
	}
	
	public Result getResult(InputStream in) throws  IOException{
		return getResult(ImageIO.read(in));
	}
	
	public String getString(InputStream in) throws  IOException{
		Result result = getResult(ImageIO.read(in));
		return (result != null) ? result.toString() : null;
	}
	
	public Result getResult(byte[] b) throws IOException{
		return getResult(ImageIO.read(new ByteArrayInputStream(b)));
	}
	
	public Result getResult(BufferedImage image) {
		Result result = null;
		int c = 0;
		logger.log(Level.INFO, ++c + " - no filtering");
		BinaryBitmap bitmap = getBitmap(image);
		result = read(bitmap);
		if (result == null) {
			logger.log(Level.INFO, ++c + " - sharpen image");
			result = read(getBitmap(sharpen(image)));
		}
		if (result == null) {
			logger.log(Level.INFO, ++c + " - rotate image");
			bitmap = bitmap.rotateCounterClockwise();
			result = read(bitmap);
		}
		if (result == null) {
			logger.log(Level.INFO, ++c + " - sharpen image");
			image = sharpen(image);
			result = read(getBitmap(image));
		}
		return result;
	}

	private Result read(BinaryBitmap bitmap) {
		Result result = null;
		Map<DecodeHintType, Object> hints = new HashMap<DecodeHintType, Object>();
		hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
		try {
			result = reader.decode(bitmap, hints);
		} catch (NotFoundException | FormatException | ChecksumException e) {
			logger.log(Level.INFO, e.getClass().getName());
		}
		return result;
	}

	private BinaryBitmap getBitmap(BufferedImage image) {
		LuminanceSource source = new BufferedImageLuminanceSource(image);
		return new BinaryBitmap(new HybridBinarizer(source));
	}

	private BufferedImage sharpen(BufferedImage biIn) {
		BufferedImage biOut = new BufferedImage(biIn.getWidth(),
				biIn.getHeight(), biIn.getType());
		BufferedImageOp op = new SharpenFilter();
		op.filter(biIn, biOut);
		return biOut;
	}
	
	public static void printOutHelper(Result result) {
		Map<String, String> filter = new HashMap<String, String>();
			filter.put("DBA", "expiry date");
			filter.put("DCS", "last name");
			filter.put("DCT", "first name");
		System.out.println("format: " + result.getBarcodeFormat());
		System.out.println("error correction level: "
				+ result.getResultMetadata().get(
						ResultMetadataType.ERROR_CORRECTION_LEVEL));
		String[] ra = result.toString().split("\n");
		for (String val : ra) {
			for (String f : filter.keySet()) {
				if (val.startsWith(f)) {
					System.out.printf(filter.get(f) + ":\t%s%n",
							val.substring(3));
				}
			}
		}
	}

}
