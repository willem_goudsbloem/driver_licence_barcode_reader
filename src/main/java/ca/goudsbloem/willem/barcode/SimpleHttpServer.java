package ca.goudsbloem.willem.barcode;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

@SuppressWarnings("restriction")
public class SimpleHttpServer {

	static Logger logger = Logger.getLogger("SimpleHttpServer");

	public static void main(String[] args) throws Exception {
		HttpServer server = HttpServer.create(new InetSocketAddress(8001), 0);
		server.createContext("/api", new Handler());
		server.createContext("/*", new HtmlHandler());
		server.setExecutor(null); // creates a default executor
		server.start();
		logger.log(Level.INFO, "server running on port: "
				+ server.getAddress().getPort());
	}
	
	static class HtmlHandler implements HttpHandler {

		@Override
		public void handle(HttpExchange t) throws IOException {
			logger.log(Level.INFO, "html");
			FileInputStream in = new FileInputStream("public/index.html");
			byte[] b = new byte[1024];
			int read;
			int length = 0;
			OutputStream os = t.getResponseBody();
			while ((read = in.read(b)) != -1) {
				os.write(b, 0, read);
				os.flush();
				length++;
			}
			t.getResponseHeaders().add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, text/html, Accept");
			t.getResponseHeaders().add("Access-Control-Allow-Methods", "GET,OPTIONS");
			t.sendResponseHeaders(200, length);
			in.close();
			os.close();			
		}
		
	}

	static class Handler implements HttpHandler {
		public void handle(HttpExchange t) throws IOException {
			logger.log(Level.INFO, t.getRequestMethod());
			String response = "This is the response";
			if (t.getRequestMethod().equals("POST")) writeToFile(t.getRequestBody());
			t.getResponseHeaders().add("Access-Control-Allow-Methods", "POST,OPTIONS");
			t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
			t.getResponseHeaders().add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
			t.sendResponseHeaders(200, response.length());
			OutputStream os = t.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}

		private void writeToFile(InputStream in) {
			try {
				// dlbr.getResult(bb.array());
				FileOutputStream fos = new FileOutputStream(
						"barcodes/my-image3-" + (new Date()).getTime() + ".png");
				byte[] b = new byte[1024*8];
				int read;
				while ((read = in.read(b)) != -1) {
					fos.write(b, 0, read);
					fos.flush();
				}
				fos.close();
			} catch (IOException e) {
				logger.log(Level.SEVERE, e.getMessage());
			}
		}

	}

}
